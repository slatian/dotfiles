#!/bin/bash

# Load .profile and .bashrc

[ -f $HOME/.profile ] && . $HOME/.profile
[ -f $HOME/.bashrc ] && . $HOME/.bashrc
