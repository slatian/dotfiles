export LUA_CPATH="$HOME/.local/lib/lua/?.so;;"
export LUA_PATH="$HOME/.local/lib/lua/?.lua;$HOME/.luarocks/share/lua/5.4/?.lua;;"
export PATH="$PATH:$HOME/.local/bin"

# Settings for gui frameworks
export QT_QPA_PLATFORMTHEME="qt5ct"
export GTK2_RC_FILES="$HOME/.config/gtk-2.0/gtkrc"
export BEMENU_OPTS="-n -l 20 -M 20 --nb '#222222f0' --nf '#cccccc' --tb '#111111' --tf '#aaaaaa' --fb '#111111' --ff '#dddddd' --hb '#98971A' --hf '#111111' --scrollbar autohide --scb '#222222f0' --scf '#98971A' --sb '#000000' --sf '#458588' --fn 'Noto Sans 12'"
# Some options are only supported in bemenu 0.6.8 which is not in artix yet
bemenu --help 2>&1 | grep -q border && export BEMENU_OPTS="$BEMENU_OPTS -B 5 --bdr '#000000aa' --ab '#282828f0' --af '#bbbbbb'"

# Set svdir for runits sv for userspace services

# use slightly wiered way of looking up hostname because of different distros and different shells
export SVDIR="$HOME/.config/service/$(cat /etc/hostname)"

# Set editor and browser

# Test for available texteditors in order of preference
if [ -x /usr/bin/kak ] ; then
	export EDITOR=/usr/bin/kak
elif [ -x /usr/bin/nano ] ; then
	export EDITOR=/usr/bin/nano
elif [ -x /usr/bin/vim ] ; then
	export EDITOR=/usr/bin/vim
fi

if [ -z "$TEMP$TMP" ] ; then
	export TEMP="/tmp"
	export TMP="/tmp"
fi

[ -z "$TEMP" ] && export TEMP="$TMP"
[ -z "$TMP" ] && export TMP="$TEMP"

if [ -z "$XDG_RUNTIME_DIR" ] && [ -d "$TEMP" ] ; then
	echo "Creating makeshift XDG_RUNTIME_DIR ..."
	new_xdg_runtime_dir="$TEMP/run-$(id -u)"
	if [ -d "$new_xdg_runtime_dir" ] ; then
		if [ "_$(stat -c '%a_%u' "$new_xdg_runtime_dir" )" = "_700_$(id -u)" ]; then
			export XDG_RUNTIME_DIR="$new_xdg_runtime_dir"
			echo "Reused one from earlier!"
		else
			echo "The path $new_xdg_runtime_dir is already taken and it doesn't belong to us!"
			echo "A Workaround for this is currently not implemented."
		fi
	else
		if mkdir -m 0700 "$new_xdg_runtime_dir" ; then
			export XDG_RUNTIME_DIR="$new_xdg_runtime_dir"
			echo "New runtime dir created!"
		else
			echo "Failed to create a runtime dir, $TEMP is not writable for some reason!"
		fi
	fi
	unset new_xdg_runtime_dir
fi

# fix "xdg-open fork-bomb" export your preferred browser from here
export BROWSER=firefox

# The internet was supposed to be decentalized!
export GOPROXY=direct
export GOSUMDB=off
export GOTELEMETRY=off

# Rube Goldberg Unlocker

# export RGU_UNLOCK_HOOK="$HOME/.rgu-unlock"
# export RGU_LOCK_HOOK="$HOME/.rgu-lock"
# 
# [ ! -f "$RGU_UNLOCK_HOOK" ] && touch "$RGU_UNLOCK_HOOK"
# [ ! -f "$RGU_LOCK_HOOK" ] && touch "$RGU_LOCK_HOOK"

[ -x "$HOME/.profile.local" ] && . "$HOME/.profile.local" || true
[ -x "$HOME/.profile.$HOSTNAME.local" ] && . "$HOME/.profile.$HOSTNAME.local" || true

