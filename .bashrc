#!/bin/bash
# The #! makes the linter know that this is a bash file
#
# ~/.bashrc
#

# If not running interactively, don't do the below
[[ $- != *i* ]] && return

# cat the .motd file if it exists
[ -f ~/.motd ] && cat ~/.motd

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth
HISTTIMEFORMAT="%F %T "

if command -v "slatectx-pwd" >&- 2>&-; then
	ENABLE_SLATECTX_PWD_SET="1"
	SLATECTX_PROMPT_COMMAND='[ -n "$ENABLE_SLATECTX_PWD_SET" ] && slatectx-pwd set;'
	DIRECTORY="$(slatectx-pwd get)"
	if [ "$(realpath "$PWD")" = "$(realpath "$HOME")" ] ; then
		[ -n "$DIRECTORY" ] && cd -- "$DIRECTORY"
	fi
	unset DIRECTORY
	alias cdx='cd "$(slatectx-pwd get)"'
fi

prompt_marker() {
    printf '\e]133;A\e\\'
}

# shellcheck disable=SC1091
[ -r /usr/share/bash-completion/bash_completion ] && . /usr/share/bash-completion/bash_completion

# shellcheck disable=SC1091
[ -r /usr/share/fzf/key-bindings.bash ] && . /usr/share/fzf/key-bindings.bash

# Change the window title of X terminals
case ${TERM} in
	xterm*|rxvt*|Eterm*|aterm|kterm|gnome*|interix|konsole*|foot|tmux*)
		PROMPT_COMMAND="prompt_rebuild_ps1; $SLATECTX_PROMPT_COMMAND "'echo -ne "\033]0;${USER}@${HOSTNAME%%.*}:${PWD/#$HOME/\~}\007"; prompt_marker'
		;;
	screen*)
		PROMPT_COMMAND="prompt_rebuild_ps1; $SLATECTX_PROMPT_COMMAND"' echo -ne "\033_${USER}@${HOSTNAME%%.*}:${PWD/#$HOME/\~}\033\\"; prompt_marker'
		;;
	*)
		PROMPT_COMMAND="prompt_rebuild_ps1; $SLATECTX_PROMPT_COMMAND"
		;;
esac

PROMPT_COMMAND="RETCODE=\"\$?\"; trap - DEBUG ; $PROMPT_COMMAND ; enable_debug_trap"

unset SLATECTX_PROMPT_COMMAND

# If TERM is foot and foot binary is not present set TERM to xterm
[ "$TERM" == "foot" ] && [ ! -x /usr/bin/foot ] && export TERM=xterm

start_ssh_agent () {
	eval "$(ssh-agent)" && MY_SSH_AGENT_PID="$SSH_AGENT_PID"
}

if [ -z "$SSH_AUTH_SOCK" ] ; then
	if [ -n "$XDG_RUNTIME_DIR" ] ; then
		# Lauch a global ssh agent for the user
		SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh_agent.sock"
		export SSH_AUTH_SOCK
		agent_check_result=2
		if [ -e "$SSH_AUTH_SOCK" ] ; then
			ssh-add -L >/dev/null 2>/dev/null
			agent_check_result=$?
		fi
		case "$agent_check_result" in
			2)
				echo "Starting new userwide SSH Agent."
				eval "$(ssh-agent -a "$SSH_AUTH_SOCK" -t 10800)"
				;;
		esac
		unset agent_check_result
	else
		# or at least a local one
		start_ssh_agent
	fi
fi

kill_ssh_agent () {
	[ -z "$MY_SSH_AGENT_PID" ] || kill "$MY_SSH_AGENT_PID"
}

trap kill_ssh_agent EXIT

add_ssh_key () {
	if [ -z "$1" ]; then
		ssh-add
	else
		ssh-add "$HOME/.ssh/$1"
	fi
}

alias ssa=start_ssh_agent
alias sa=add_ssh_key

TIME_SHOW_MIN=10
LAST_COMMAND_START=""

# (seconds)
format_duration() {
	local seconds minutes hours out
	seconds="$(( $1 % 60 ))"
	minutes="$(( $1 / 60 ))"
	hours="$(( $minutes / 60 ))"
	minutes="$(( $minutes % 60 ))"
	out=""
	if [ "$hours" -gt 0 ] ; then
		out="${hours}h "
	fi
	if [ -n "$out" ] || [ "$minutes" -gt 0 ] ; then
		out="${out}${minutes}m "
	fi
	printf "%s%ss\n" "$out" "$seconds"
}

prompt_last_command_duration() {
	local diff
	[ -n "$LAST_COMMAND_START" ] || return
	diff="$(( $(date --utc +%s) - $LAST_COMMAND_START ))"
	[ "$diff" -ge "$TIME_SHOW_MIN" ] || return
	printf "\033[90m[Took: %s]\033[00m" "$(format_duration "$diff")"
}

preexec_invoke_exec () {
	# Make loops somewhat efficient, only count in first event
	[ -z "$LAST_COMMAND_START" ] || return
	# Don't trigger for autocomplete
	[ -z "$COMP_LINE" ] || return
	case "$BASH_COMMAND" in
		# Avoid triggering for returncpde grab, unhook and __fzf* helpers
		"trap - DEBUG"|"RETCODE=\"\$?\""|__fzf*) return;;
	esac
	LAST_COMMAND_START="$(date --utc +%s)"
}

enable_debug_trap() {
	LAST_COMMAND_START=""
	trap 'preexec_invoke_exec' DEBUG
}

BRIGHT_RED="\033[01;31m"
DEFAULT_COLOR="\033[00m"

if [ -n "$(ls /sys/class/power_supply/BAT*/capacity /sys/class/power_supply/*-battery/capacity 2> /dev/null)" ] ; then
	# shellcheck disable=SC2016 # battcap isn't supposed to be exanded here.
	BATTERY_PS1='\[\033[00;37m\]$(battcap)\[\033[30m\]-'
fi

battcap () {
	cat /sys/class/power_supply/BAT*/capacity /sys/class/power_supply/*-battery/capacity 2> /dev/null | tr '\n' '+' | sed 's/+$//'
}

retcode () {
	if [[ "$RETCODE" != 0 ]] ; then
		printf "[%s]" "$RETCODE"
	fi
}

prompt_git_branch() {
	command -v git >/dev/null 2>/dev/null || return
	branchlist="$(git branch --list 2>/dev/null)"
	if [ "$(printf "%s\n" "$branchlist" | wc -l)" -gt 1 ] ; then
		printf "\x1b[37m[\x1b[00;33m󰘬(%s)\x1b[37m]" "$(git branch --list | grep '^\* ' | sed 's/^..//')"
	fi
}

strip_escape_sequences() {
	sed -e 's/\x1b\[[^m]*m//g' -e 's/\x1b//g'
}

escape_for_prompt() {
	sed -E \
		-e 's/[\$\{\}`]/\\\0/g' \
		-e 's/\x1b([^m]+)m/\\[\\033\1m\\]/g' \
		-e 's/\\\{\\\{newline\\\}\\\}/\\n/g'
}

# short 2 deep pwd command for the prompt
PWDSHORT_HOME_REP="~"
pwdshort () {
	echo "${PWD/#$HOME/$PWDSHORT_HOME_REP}" | tr / '\n' | tail -n 2 | tr '\n' / | sed 's/\/$//'
}

responsive_newline() {
	[[ "$(tput cols 2> /dev/null)" -gt 50 ]] || printf "\n "
}

PS1_REMOTE_MARKER=""
[ -z "$SSH_CONNECTION" ] || PS1_REMOTE_MARKER="\[\033[00;01;32m\][ssh]"
[ -z "$SUDO_UID" ] || PS1_REMOTE_MARKER="\[\033[00;01;32m\][sudo]"

PS1_USER_COLOR="\033[33m"
PS1_MAIN_BRACKET_COLOR="\033[00m"
PS1_END="\[\033[00;32m\]$"
if [ "$EUID" = 0 ] ; then
	PS1_USER_COLOR="\033[01;31m"
	PS1_MAIN_BRACKET_COLOR="\033[01;31m"
	PS1_END="#"
fi


prompt_rebuild_ps1() {
	widgets="$(prompt_last_command_duration)$(prompt_git_branch)"
	[ -z "$widgets" ] || widgets="$(printf %s{{newline}} "$widgets" | escape_for_prompt)"
	PS1="$widgets\[\033[01;31m\]$(retcode | escape_for_prompt)${PS1_REMOTE_MARKER}\[${PS1_MAIN_BRACKET_COLOR}\][${BATTERY_PS1}\[${PS1_USER_COLOR}\]\u\[\033[01;31m\]@\[\033[00;32m\]\h\[\033[00m\]:\[\033[01;34m\]$(pwdshort | escape_for_prompt)\[${PS1_MAIN_BRACKET_COLOR}\]]$(responsive_newline)${PS1_END}\[\033[00m\] "
}

prompt_rebuild_ps1

# set ls colorscheme
LS_COLORS='rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:mi=00:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arc=01;31:*.arj=01;31:*.taz=01;31:*.lha=01;31:*.lz4=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.tzo=01;31:*.t7z=01;31:*.zip=01;31:*.z=01;31:*.dz=01;31:*.gz=01;31:*.lrz=01;31:*.lz=01;31:*.lzo=01;31:*.xz=01;31:*.zst=01;31:*.tzst=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.alz=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.cab=01;31:*.wim=01;31:*.swm=01;31:*.dwm=01;31:*.esd=01;31:*.jpg=01;35:*.jpeg=01;35:*.mjpg=01;35:*.mjpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.webp=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.m4a=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.oga=00;36:*.opus=00;36:*.spx=00;36:*.xspf=00;36:';
export LS_COLORS
export USER_LS_COLORS="$LS_COLORS"

if grep --help 2>&1 | grep -q '.--color' ; then
	alias grep='grep --color=auto'
fi

ip --help 2>&1 | grep -q '.-c.olor' && alias ip='ip -c'

alias cp="cp -i"                          # confirm before overwriting something
alias df='df -h'                          # human-readable sizes
alias free='free -m'                      # show sizes in MB
alias ..='cd ..'
alias blobcat=xxd
alias ap=ansible-playbook
alias sudi="sudo -i"
alias ls='ls --color=auto'
alias ll='ls -lh  --color=auto'
alias la='ls -lah --color=auto'
alias r='slate-rename'
command -v xbps-query > /dev/null 2> /dev/null && alias xbps-search='xbps-query -R -s'
alias ffh='ssh -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null"'
alias fcp='scp -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null"'
alias dotgit='git --git-dir="$HOME/.dotgit" --work-tree="$HOME"'

# useful for testing multifile scripts
alias here='PATH="$PWD:$PATH"'

alias cargo-bin='PATH="$HOME/.cargo/bin/:$PATH"'

command -v kak-slatectx > /dev/null 2> /dev/null && alias kak='kak-slatectx'

#xhost +local:root > /dev/null 2>&1

complete -cf sudo
complete -cf doas

# Bash won't get SIGWINCH if another process is in the foreground.
# Enable checkwinsize so that bash will check the terminal size when
# it regains control.  #65623
# http://cnswww.cns.cwru.edu/~chet/bash/FAQ (E11)
shopt -s checkwinsize

shopt -s expand_aliases

# export QT_SELECT=4

# Enable history appending instead of overwriting.  #139609
shopt -s histappend

# Enable directory switching without cd
shopt -s autocd

# Do a proper clear for Ctrl+l
bind '"\C-l": clear-display'

#
# # ex - archive extractor
# Thanks Manjaro!
# # usage: ex <file>
ex ()
{
  if [ -f "$1" ] ; then
    case "$1" in
      *.tar.bz2)   tar xjf "$1"   ;;
      *.tar.gz)    tar xzf "$1"   ;;
      *.bz2)       bunzip2 "$1"   ;;
      *.rar)       unrar x "$1"     ;;
      *.gz)        gunzip "$1"    ;;
      *.tar)       tar xf "$1"    ;;
      *.tbz2)      tar xjf "$1"   ;;
      *.tgz)       tar xzf "$1"   ;;
      *.zip)       unzip "$1"     ;;
      *.Z)         uncompress "$1";;
      *.7z)        7z x "$1"      ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

# better yaourt colors
export YAOURT_COLORS="nb=1:pkg=1:ver=1;32:lver=1;45:installed=1;42:grp=1;34:od=1;41;5:votes=1;44:dsc=0:other=1;35"

# jq theme
export JQ_COLORS="1;31:0;39:0;39:0;39:0;32:1;39:1;39"

# Chainload a .bashrc.local if it exists
# shellcheck disable=2015 disable=1090
[ -x ~/.bashrc.local ] && . ~/.bashrc.local || true
