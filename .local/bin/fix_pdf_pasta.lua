#!/bin/lua5.4

local acc = ""

while true do
	local i = io.read()
	if not i then break end
	if acc:match("[a-z]%-$") then
		acc = acc:gsub("%-$", "")..i
	elseif acc:match("[a-z]\xC2\xAD$") then
		acc = acc:gsub("..$", "")..i
	elseif acc == "" then
		acc = i
	else
		acc = acc.." "..i
	end

	if i:match("[.!?]%s*$") then
		print(acc)
		acc = ""
	elseif i:match("^%s*$") then
		print(acc.."\n")
		acc=""
	end
end

print(acc)
