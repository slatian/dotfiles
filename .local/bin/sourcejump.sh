#!/bin/bash

set -e

show_help() {
cat <<EOF
Usage: sourcejump.sh [options]

Sourcejump will try to extract a filepath and a line:column position from a given piece of text, try to find that file and open it in your favorite text-editor.

--text|-t <text>
	Set the text to process manually, falls back to the output of wl-paste -p.
--position <line>[:<column>]
	Manually specify a position to use, if set the script will not attempt to extract a position.
--filepath <path>
	Filepath that will be used in place of an extracted one.

--no-search
	Disable searching, either the path points to a file, or it does not.
--searchpath <path>
	Set the path to search for the file, defaults to the pwd.
--search-timeout <seconds>
	Sets the timeout in seconds before the find process gets killed when it doesn't find anything quickly, defaults to 2 seconds.
--search-file <path-pattern>
	Search for a file matching the given path glob and output the path when found, automatically expands the scope when the file is not found under the searchpath.

--opener <command>
	The command to use for opening the found file.
	Default: kak-e
--opener-file-option <option>
	The option used to tell the opener command that whatever follows is a filepath to a file it should open.
	Default: --file
--opener-linedesc-option <option>
	The option used to tell the opener that it should jump to a particular line:column
	Defaults to being empty.
--opener-linedesc-prefix <prefix>
	What to prepend to the linedesc to make the opener recognize it as one.
	Default: +

--dry-run
	Don't run the opener, useful for debugging.
--verbose|-v
	Become more chatty.
-vd
	Short for --dry-run and --verbose.
--json
	Print the extracted information as json insteadof running the opener.

Note: The contructed command will look a bit like this:
	<opener> [<file-option>] <filepath> [[<linedesc-option>] <linedesc-prefix><line>[:<col>]]
EOF
}

TEXT=""
POSITION=""
FILEPATH=""
json="{}"

do_search="y"
searchpath="."
search_timeout="2"
file_option="--file"
linedesc_prefix="+"
linedesc_option=""
opening_command="kak-e"
dry_run=""
# imprtable via envoirnment
verbose="${verbose:-}"
output_json=""
path_pattern=""

while [[ "$#" -gt 0 ]]; do
	case "$1" in
		--no-search) do_search=""; shift 1;;
		--searchpath) searchpath="$2"; shift 2;;
		--search-timeout) search_timeout="$2"; shift 2;;
		--search-file) path_pattern="$2"; shift 2;;
		
		--opener) searchpath="$2"; shift 2;;
		--opener-file-option) file_option="$2"; shift 2;;
		--opener-linedesc-option) linedesc_option="$2"; shift 2;;
		--opener-linedesc-prefix) linedesc_prefix="$2"; shift 2;;
		
		--text|-t) TEXT="$2"; shift 2;;
		--position) POSITION="$2"; shift 2;;
		--filepath) FILEPATH="$2"; shift 2;;
		
		--dry-run) dry_run="y"; shift 1;;
		--verbose|-v) verbose="y"; shift 1;;
		-vd) verbose="y" ; dry_run="y"; shift 1;;
		--json) output_json="y"; shift 1;;
		
		--help) show_help; exit 0;;
		*) printf "Unknown option: %s\n" "$1" >&2 ; exit 1;;
	esac
done

cd "$searchpath"

# level, printf format, printf args
function log() {
	level="$1"
	format="${2:-%s}"
	shift 2
	case "$level" in
		debug)
			if [ -n "$verbose" ] ; then
				printf "[%s] $format\n" "$level" "$@" >&2 || true
			fi
			;;
		*)
			printf "[%s] $format\n" "$level" "$@" >&2 || true
			;;
	esac
}


# key, value, type
function set_val(){
	json="$(printf "%s" "$json" | jq ".$1=\$v" "--arg${3}" v "$2")"
}

# (searchpath, path_pattern)
search_file() {
	cd "$1"
	while [ "$PWD" != "/" ] ; do
		[ -z "$verbose" ] || log debug "Searching directory: %s" "$PWD"
		FILE="$(find . -type f -maxdepth 10 -path "$2" 2>&- | head -n 1 || true)"
		if [ -n "$FILE" ] ; then
			log info "Found file at: %s" "$FILE"
			slate-realpath -- "$FILE"
			break
		fi
		cd ..
	done
}

# handle search option
if [ -n "$path_pattern" ] ; then
	log debug "Searching for a path matching: %s" "$path_pattern"
	search_file "$searchpath" "$path_pattern"
	exit
fi

if [ -z "$TEXT" ] && [ -n "$WAYLAND_DISPLAY" ] ; then
	log debug "Falling back to wayland primary clipboard."
	TEXT="$(wl-paste -p)"
fi

# Inputs for testing:
#
## Rust compiler
#   --> src/main.rs:27:5
#
## Rust panic
#	thread 'main' panicked at crawler/src/token_index_rebuilder.rs:69:91:
#
## Bash
# .local/bin/sourcejump.sh: line 82: syntax error near unexpected token `|'
#
## Sh
# ./xdg-open: 757: kfmclient: not found
#
## Python
# File "/home/slatian/not_working.py", line 1
#
## git diff
# --- a/.local/bin/sourcejump.sh
# +++ b/.local/bin/sourcejump.sh
#
## lua
# lua: not_working.lua:1: syntax error near '*'

# Pre cleanup
# * Remove empty lines
# * discard everything after the second line
# * strip text that doesn't contain useful information for us
# 	* remove arrows (rust, '-->') and  colons (':::')
# 	* remove 'lua: ' prefix
# 	* remove 'File "' prefix
# 	* remove 'a/', '--- a/', 'b/' and '+++ b/' for git diff
# 	* remove leading and trailing spaces
# 	* remove ':…' 'error:', 'warning:', 'info:', 'in'
# 	  constructs usually providing explanations
TEXT="$(
	printf "%s" "$TEXT" |
	sed '/^\s*$/d' |
	head -n 2 |
	sed -E \
		-e 's/ (-*>|:::) //' \
		-e 's/^lua: //' \
		-e 's/^In //' \
		-e 's/^File \"//' \
		-e 's|^[ab]/||' \
		-e 's/^(\-\-\- a|\+\+\+ b)\///' \
		-e 's/^ *//' -e 's/ *$//' \
		-e 's/:? (error:|warning:|info:|note:|in ).*//' \
		-e 's/.* panicked at //' \
		-e 's/:$//'
)"

if [ -z "$TEXT" ] ; then
	log error "Text cleanup didn't leave anything that could be a filepath."
	exit 1
fi

log debug "Cleaned up text: %s" "$TEXT"
set_val cleaned_text "$TEXT"

if [ -z "$FILEPATH" ] ; then
	log debug "Extracting filepath …"
	# Extract filepath
	# * filepath must be in first line
	# * strip inwanted suffixes
	# 	* Throw away '", ' and everything after,
	# 	  it terminates a filepath on python messages
	# 	* Throw away optinal colons followed by optional spaces
	# 	  and something that looks like a line specification
	# 	  before the end of the text
	# 	* Throw away everything after the first space
	# 	  plus optional columns that precede it
	# 	* Throw away a ':<number>' construct that may still hang onto the filename
	FILEPATH="$(printf "%s" "$TEXT" | head -n 1 | sed -E -e 's/\", .*$//' -e 's/:* *[0-9]*:?[0-9]*$//' -e 's/:*\s.*$//' -e 's/:[0-9]*//' )"

	if [ -z "$FILEPATH" ] ; then
		log error "Could not extract a filepath."
		if [ -n "$output_json" ] ; then
			printf "%s\n" "$json"
		fi
		exit 1
	fi

	log debug "Extracted filepath: %s" "$FILEPATH"
	set_val extracted_path "$FILEPATH"
fi

if [ -z "$POSITION" ] ; then
	log debug "Extracting position …"
	# Try to get the fist thing that looks like a colon followed by a numbers (followed by an optional colon + numbers) from the first line
	POSITION="$(printf "%s" "$TEXT" | head -n 1 | grep -oP ': ?[0-9]*:?[0-9]*' | sed -e 's/^://' -e 's/^ //' | head -n 1)"
	# Same as abobe, but the prefix is 'line '
	[ -n "$POSITION" ] || POSITION="$(printf "%s" "$TEXT" | grep -oP 'line [0-9]*:?[0-9]*' | sed 's/^line *//' | head -n 1)"
	# Give up and search for the first number followed by an optional colon + number
	[ -n "$POSITION" ] || POSITION="$(printf "%s" "$TEXT" | grep -oP '[0-9]*:?[0-9]*' | head -n 1)"
	log debug "Extracted Position: %s" "$POSITION"
fi

if [ -n "$POSITION" ]; then
	if ! grep -qE '^[0-9]*:?[0-9]*$' <<< "$POSITION" ; then
		log warning "Extracted position does not match the expected format of [<line>][:<column>], dicarding it."
		POSITION=""
	else
		log info "Position: %s" "$linedesc_prefix$POSITION"
		set_val position "$POSITION"
		if [ -n "$output_json" ] ; then
			set +e
			line="$(printf "%s" "$POSITION" | grep -o '^[0-9]*')"
			col="$(printf "%s" "$POSITION" | sed -E 's/^[0-9]*:?//')"
			set -e
			set_val line "${line:-null}" json
			set_val column "${col:-null}" json
		fi
	fi
else
	log info "No useable position file found."
fi


if [ -f "$FILEPATH" ] ; then
	log info "Filepath directly points to a file in pwd."
elif [ -n "$do_search" ] ; then
	log info "Searching for a file …"
	path_pattern="*$(printf "%s" "$FILEPATH" | sed -e 's/\\/\\\\/g' -e 's/\*/\\*/g' )"
	searchpath="$(slate-realpath -- "$searchpath")"
	export verbose
	FILEPATH="$(timeout "$search_timeout" "$0" --searchpath "$searchpath" --search-file "$path_pattern")"
fi

if ! [ -f "$FILEPATH" ] ; then
	log error "Unable to find a matching file!"
	if [ -n "$output_json" ] ; then
		printf "%s\n" "$json"
	fi
	exit 1
fi

log info "File: %s" "$FILEPATH"
set_val file "$FILEPATH"

if [ -n "$output_json" ] ; then
	printf "%s\n" "$json"
	exit 0
fi

log debug "Assembling command …"
command_acc=("$opening_command")
if [ -n "$file_option" ] ; then command_acc+=("$file_option") ; fi
command_acc+=("$FILEPATH")
if [ -n "$linedesc_option" ] ; then command_acc+=("$linedesc_option") ; fi
if [ -n "$POSITION" ] ; then command_acc+=("$linedesc_prefix$POSITION") ; fi

log info "Command: %s" "${command_acc[*]}"

if [ -z "$dry_run" ] ; then
	log debug "Running command …"
	"${command_acc[@]}"
else
	log debug "Dry run, not executing command."
fi
