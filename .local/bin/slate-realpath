#!/bin/sh

set -e

action="realpath"
realpath_backend="auto"
no_symlinks=""

show_help() {
cat <<EOF
Usage: $(basename "$0") [OPTIONS ...] [--] FILE ...

Options:

-s, --no-symlinks
	Don't expand symlinks

-h, --help
	Shows this help text.
	
--get-backend
	Show which backend

--force-backend <backend>
	Force the script to use a specific backend for testing purposes,
	
Supported Backends:
* gnu-realpath
* busybox-realpath
* readlink
* normalize
EOF
}

check_options() {
	while [ $# -gt 0 ] ; do
		case "$1" in
			-s|--no-symlinks)
				no_symlinks=y
				shift 1 ;;
			-h|--help)
				show_help
				exit 0 ;;
			--get-backend)
				action="get-backend"
				shift 1 ;;
			--force-backend)
				realpath_backend="$2"
				shift 2 ;;
			--)
				break ;;
			-*)
				echo "Unknown option '$1'"
				exit 1 ;;
			*)
				shift 1 ;;
		esac
	done
}

check_options "$@"

if [ "$realpath_backend" = "auto" ] ; then
	if command -v realpath >/dev/null 2>/dev/null ; then
		lines="$(realpath -- / 2>&1 | wc -l)"
		if [ "$lines" = 1 ] ; then
			realpath_backend="gnu-realpath"
		elif [ -z "$no_symlinks" ] ; then
			realpath_backend="busybox-realpath"
		else
			realpath_backend="normalize"
		fi
	elif [ -n "$no_symlinks" ] ; then
		realpath_backend="normalize"
	elif command -v readlink >/dev/null 2>/dev/null ; then
		realpath_backend="readlink"
	else
		echo "Operation impossible, no realpath backend." >&2
		exit 3
	fi
fi

if [ "$action" = get-backend ] ; then
	printf "%s\n" "$realpath_backend"
	exit
fi

normalize_path() {
	if printf "%s" "$1" | grep -q "^/" ; then
		path="$1/"
	else
		path="$PWD/$1/"
	fi

	newpath=""
	IFS=/
	for p in $path ; do
		case "$p" in
			""|.) ;;
			..)
				if [ -n "$newpath" ] ; then
					newpath="$(printf %s "$newpath" | sed 's|/[^/]*$||')"
				else
					echo "realpath: $1: No such file or directory" >&2
					return 1
				fi
				;;
			*)
				newpath="$newpath/$p" ;;
		esac
	done
	printf "%s\n" "$newpath" | sed 's|/$||'
}

run_realpath() {
	case "$realpath_backend" in
		gnu-realpath)
			if [ -z "$no_symlinks" ] ; then
				realpath -- "$1"
			else
				realpath -s -- "$1"
			fi
			;;
		busybox-realpath)
			realpath "$1" ;;
		readlink)
			readlink -f "$1" ;;
		normalize)
			normalize_path "$1" ;;
		*)
			echo "Operation impossible, unknown backend." >&2
			exit 3 ;;
	esac
}

is_past_doubledash=""
while [ $# -gt 0 ] ; do
	if [ -z "$is_past_doubledash" ] ; then
		case "$1" in
			--) shift 1 ; is_past_doubledash="y" ;;
			--force-backend) shift 2 ;;
			-*) shift 1 ;;
			*) run_realpath "$1" ; shift 1 ;;
		esac
	else
		run_realpath "$1"
		shift 1
	fi
done
