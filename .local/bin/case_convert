#!/bin/lua5.3

function from_camel_case(text)
	local out = {}
	--make lowerCamel work
	text:gsub("^[^%u]+", function (s) out[#out+1] = s end )
	text:gsub("%u+[^%u]+", function (s) out[#out+1] = s end )
	return out
end

function from_kebab_case(text)
	local out = {}
	text:gsub("[^-]+", function (s) out[#out+1] = s end )
	return out
end

function from_snake_case(text)
	local out = {}
	text:gsub("[^_]+", function (s) out[#out+1] = s end )
	return out
end

function from_space_seperated(text)
	local out = {}
	text:gsub("[^%s]+", function (s) out[#out+1] = s end )
	return out
end

function titlecase_word(word)
	local word = word:lower():gsub("^.", string.upper)
	return word
end

function no_filter(word, i)
	return word
end

function dromedar_filter(word,i)
	if i > 1 then
		return titlecase_word(word)
	else
		return word:lower()
	end
end

function join_string_array(words, merger, filter, if_pattern, else_merger)
	local out = ""
	local filter = filter or no_filter
	for i,w in ipairs(words) do
		if w:match(if_pattern or ".") then
			if i > 1 then out = out..merger end
			out = out..filter(w,i)
		else
			if i > 1 then out = out..else_merger end
			out = out..w
		end
	end
	return out
end

local commands = {...}
local from_commands = {}
local to_commands = {}
in_block = "default"

for _,c in ipairs(commands) do
	if c == "to" then
		if in_block == "default" then
			from_commands = to_commands
			to_commands = {}
		end
		in_block = "to"
	elseif c == "from" then
		in_block = "from"
	elseif in_block == "to" or in_block == "default" then
		to_commands[#to_commands+1] = c
	elseif in_block == "from" then
		from_commands[#from_commands+1] = c
	end
end

rules = {
	upper = {
		filter = string.upper,
		match_word = "[%u%d]+"
	},
	lower = {
		filter = string.lower,
		match_word = "[%l%d]+",
	},
	title = {
		filter = titlecase_word,
		match_word = "%u[%l%d]*"
	},
	any = {
		match_word = "[%w]+"
	},
	space = {
		seperator = from_space_seperated,
		merger = " ",
		match_seperator = " ",
		match_unit = "%w[%w ]*"
	},
	kebab = {
		seperator = from_kebab_case,
		merger = "-",
		match_seperator = "%-",
		match_unit = "%w+%-[%w%-]+"
	},
	snake = {
		seperator = from_snake_case,
		merger = "_",
		match_seperator = "%_",
		match_unit = "%w+%_[%w%_]+"
	},
	camel = {
		seperator=from_camel_case,
		merger = "",
		filter = titlecase_word,
		match_unit = "%u+[%l%d]+%u[%l%d%u]*"
	},
	dromedar = {
		seperator = from_camel_case,
		merger = "",
		filter = dromedar_filter,
		match_unit = "[%l%d]+%u[%l%d%u]*"
	},
	case  = {},
	seperated = {},
}

rules.screaming = rules.upper
rules.hyphen = rules.kebab
rules.pascal = rules.camel

function merge_rule(configuration, rule)
	for k,v in pairs(rule) do
		--print("Setting:", k, v)
		configuration[k] = v
	end
end

function parse_commands(commands)
	local configuration = {}
	for _,c in ipairs(commands) do
		local rule = rules[c:lower()]
		assert(rule, "Unknown keyword: "..c)
		merge_rule(configuration, rule)
	end
	return configuration
end

from = parse_commands(from_commands)
to = parse_commands(to_commands)

while true do
	local i = io.read()
	if not i then break end

	-- automaticcally determine word seperator if none is given
	if to.merger and not from.match_unit then
		if i:match("%w %w") then
			merge_rule(from,rules.space)
		elseif i:match("%w%_%w") then
			merge_rule(from,rules.snake)
		elseif i:match("%w%-%w") then
			merge_rule(from,rules.kebab)
		-- a camel case unit without the uppercase letter at the start is a dromedar unit
		elseif i:match(rules.dromedar.match_unit) then
			if i:match("^%s*%u") then
				merge_rule(from, rules.camel)
			else
				merge_rule(from, rules.dromedar)
			end
		end
	end

	-- print(string.upper(to_snake_case(from_camel_case(i))))
	local match_unit = from.match_unit or from.match_word or "%w+"
	local out = i:gsub(match_unit, function(unit)
		if from.seperator then
			local array = from.seperator(unit)
			return join_string_array(
				array,
				to.merger or from.merger or "_",
				to.filter,
				from.match_word,
				from.merger or " "
			)
		elseif to.filter and not from.match_unit then
			return to.filter(unit)
		end
		return unit
	end)
	print(out)
end
