#!/bin/bash

url_regex='[a-z+-]*://[-A-Za-z0-9.:@]*/'

YTDL_FORMAT="[height<=1080]"

url="$1"
[[ -z "$url" ]] && url="$(wl-paste)"

if ! [[ $url =~ $url_regex ]] ; then
	notify-send -a "Downloader" "That doen't look like an url to me" "$url"
	exit 1
fi

choice="$(printf "Download Video\nDownload Music\nPlay with MPV" | bemenu -p "Do the following")"

choose_directory() {
	find . -mindepth 1 -type d | sed s/^\\.\\///  | bemenu -i -p "$choice to"
}

run_youtube_dl() {
	notifiction_id="$(notify-send -p -a "Downloader" "downloading ..." "$url\nto: $(pwd)")"
	youtube-dl "$@" 2>&1 | tr -d '[:cntrl:]' | tr '[' '\n' | sed '/^K$/d' | sed 's/^/[/' | xargs -i notify-send -r "$notifiction_id" -t 60 "downloading ..." "{}" && notify-send  -r "$notifiction_id" "Finished: downloading" "$url"
}

case "$choice" in
	"Play with MPV")
		mpv --ytdl-format="[height<=1080]" "$url"
		;;
	"Download Video")
		cd "$HOME/Videos"
		category="$(choose_directory)"
		if [[ -n "$category" ]] ; then
			cd "$category"
			run_youtube_dl -f "$YTDL_FORMAT" -- "$url"
		else
			notify-send -a "Downloader" "Dwnload Cancelled"
			exit 2
		fi
		;;
	"Download Music")
		cd "$HOME/Music"
		category="$(choose_directory)"
		if [[ -n "$category" ]] ; then
			cd "$category"
			run_youtube_dl -x -- "$url"
			mpc update || true
		else
			notify-send -a "Downloader" "Dwnload Cancelled"
			exit 2
		fi
		;;
	*)
		notify-send -a "Downloader" "Dwnload Cancelled"
		;;
esac
