#!/bin/bash

class=$1
workspace=$2
command=$3

classtype=app_id

if [ -z "$WAYLAND_DISPLAY" ] ; then
	classtype=class
	alias swaymsg=i3-msg
fi

if swaymsg '['$classtype'="'$class'"] focus'
then
	echo "focussed"
else
	if [ "$workspace" != "NONE" ]
	then
		swaymsg workspace "$workspace"
	fi
	$command
fi
