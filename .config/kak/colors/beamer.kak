# Solarized Light

evaluate-commands %sh{
	base03='rgb:002b36'
	base02='rgb:073642'
	base01='rgb:586e75'
	base00='rgb:657b83'
	base0='rgb:839496'
	base1='rgb:93a1a1'
	base2='rgb:eee8d5'
	base3='rgb:fdf6e3'
	yellow='rgb:b58900'
	orange='rgb:cb4b16'
	red='rgb:dc322f'
	magenta='rgb:d33682'
	violet='rgb:6c71c4'
	blue='rgb:268bd2'
	cyan='rgb:2aa198'
	green='rgb:859900'

    echo "
        # code
        face window value              ${cyan}
        face window type               ${red}
        face window variable           ${blue}
        face window module             ${cyan}
        face window function           ${blue}
        face window string             ${cyan}
        face window keyword            ${green}
        face window operator           ${yellow}
        face window attribute          ${violet}
        face window comment            ${base1}
        face window documentation      comment
        face window meta               ${orange}
        face window builtin            default+b

        # markup
        face window title              ${blue}+b
        face window header             ${blue}
        face window mono               ${base1}
        face window block              ${cyan}
        face window link               ${base01}
        face window bullet             ${yellow}
        face window list               ${green}

        # builtin
        face window Default            ${base00},${base3}
        face window PrimarySelection   ${base3},${blue}+fg
        face window SecondarySelection ${base1},${base01}+fg
        face window PrimaryCursor      ${base3},${base00}+fg
        face window SecondaryCursor    ${base3},${base1}+fg
        face window PrimaryCursorEol   ${base3},${yellow}+fg
        face window SecondaryCursorEol ${base3},${orange}+fg
        face window LineNumbers        ${base1},${base2}
        face window LineNumberCursor   ${base01},${base2}
        face window LineNumbersWrapped ${base2},${base2}
        face window MenuForeground     ${base3},${yellow}
        face window MenuBackground     ${base01},${base2}
        face window MenuInfo           ${base1}
        face window Information        ${base2},${base1}
        face window Error              ${red},default+b
        face window DiagnosticError    ${red}
        face window DiagnosticWarning  ${yellow}
        face window StatusLine         ${base01},${base2}+b
        face window StatusLineMode     ${orange}
        face window StatusLineInfo     ${cyan}
        face window StatusLineValue    ${green}
        face window StatusCursor       ${base0},${base03}
        face window Prompt             ${yellow}+b
        face window MatchingChar       ${red},${base2}+b
        face window BufferPadding      ${base1},${base3}
        face window Whitespace         ${base1}+f
    "
}

