hook global WinSetOption filetype=yaml %{
	set-option buffer indentwidth 2
	map buffer insert <tab> '<a-;><a-gt>'
	map buffer insert <s-tab> '<a-;><a-lt>'

	set-option window lintcmd "yamllint -f parsable"
}
