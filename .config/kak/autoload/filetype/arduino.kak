# Detection

# For now lets limit outselfes to recognizing the *.ino files as c++ sources
hook global BufCreate .*\.ino$ %{
    set-option buffer filetype cpp
}
