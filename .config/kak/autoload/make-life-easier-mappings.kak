map -docstring 'mode-switch to insert mode' global normal '<c-space>' 'i'
map -docstring 'mode-switch to global mode' global insert '<c-space>' '<esc>'

map global insert '<c-b>' '<c-p>' # That is more convenient to reach

# I want to keep the habit of saving my work
# (there was a thunderstorm, a power outage, a safe-dialog and some unfortunate timing …)
map global insert '<c-s>' '<a-;>:w<ret>'
map global normal '<c-s>' ':w<ret>'

map global normal '<c-s-left>' 'B'
map global normal '<c-s-right>' 'E'
map global normal '<c-s-up>' '<s-up>'
map global normal '<c-s-down>' '<s-down>'

map global insert '<c-left>' '<esc>bi'
map global insert '<c-right>' '<esc>ea'

map global insert '<c-left>' '<esc>bi'
map global insert '<c-right>' '<esc>ea'

map global normal '<c-tab>' ':fzf-buffer<ret>'
map global normal '<a-tab>' ':fzf-edit<ret>'

map global normal '<c-ret>' ':lint-buffer<ret>'

# bring back the old X behaviour (tiny little edge case remains)
map global normal 'X' 'Ex'

# Move the selection one line up or down
map global normal '<a-up>' 'xdkP'
map global normal '<a-down>' 'xdp'

map global normal '<a-right>' 'dep'
map global normal '<a-left>' 'dbP'

map global normal '<c-.>'  ':lsp-hover'
map global insert '<c-.>'  '<esc>:lsp-hover<ret>i'
