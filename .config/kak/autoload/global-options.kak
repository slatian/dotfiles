set-option global tabstop 4
set-option global indentwidth 0

set-option -add current static_words local
set-option -add current static_words global

#clippy is nice but sometimes I'm on my phone
set-option -add current ui_options terminal_assistant=none
# the mouse is better used for selecting text to the primary clipboard
define-command mouse %{ set-option -add current ui_options terminal_enable_mouse=yes }
define-command no-mouse %{ set-option -add current ui_options terminal_enable_mouse=no }

#colorscheme tomorrow-night
colorscheme slatian-gruvbox

# I like line numbers and I don't mind them being everywhere
add-highlighter global/numbers number-lines -hlcursor
add-highlighter global/wrap wrap -word -indent

#TODO: add highlighter that uses the dynregex highlighter to find selections
#Search highlighter taken from the examples
add-highlighter global/search dynregex '%reg{/}' 0:+i
add-highlighter global/matching show-matching

