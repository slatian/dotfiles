# declare the option current_client for use by external scripts to grab the last active lcient on a kakoune server, i.e. to open and show a document from the filemanager

# Opening a file:
# printf 'evaluate-commands -client "%%opt{current_client}" %%\0edit %%\0\0%s\0\0\0' "example.txt" | kak -p foo

declare-option str current_client client0

hook global FocusIn .* %{
	set global current_client %val{client}
}

