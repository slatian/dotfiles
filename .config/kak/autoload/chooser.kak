# taken from the examples

define-command -docstring 'Invoke fzf to open a file' -params 0 fzf-edit %{
    evaluate-commands %sh{
		file=""
	    if [ -n "$WAYLAND_DISPLAY" ]; then
			# We are running inside a wayland session!
			file="$(find . -type f ! -path '*/.*' | head -n 1000 | bemenu -l 25 -p "open")"
        elif [ -z "${kak_client_env_TMUX}" ]; then
            printf 'fail "client was not started under tmux"\n'
        else
            file="$(find . -type f ! -path '*/.*' | head -n 1000 |TMUX="${kak_client_env_TMUX}" fzf-tmux -d 15)"
        fi
        if [ -n "$file" ]; then
            printf 'edit "%s"\n' "$file"
        fi
    }
}


# the original version no longer works since kak_buflist is no longer ":" separated.
# this one works even you have single quote or newline in file names.

define-command -docstring 'Invoke fzf to select a buffer' fzf-buffer %{
    evaluate-commands %sh{
		
        BUFFER="$(
            (
                eval "set -- $kak_quoted_buflist"
                while [ $# -gt 0 ]; do
                    printf "%s\0" "$1"
                    shift
                done
            ) |
            if [ -n "$WAYLAND_DISPLAY" ]; then
            	#breaks when filname contains a newline but works for all other cases
				tr '\0\n' '\n\0' | bemenu -p "buffer"
            else
	            fzf-tmux -d 15 --read0
	        fi
        )"
        if [ -n "$BUFFER" ]; then
            printf "buffer '%s'" "$( printf "%s" "$BUFFER" | sed "s/'/''/" )"
        fi
    }
}
