

#Wayland integration, Thanks timber!
map -docstring 'paste wayland clipboard after cursor' global user 'p' '<a-!>wl-paste -n<ret>'
map -docstring 'paste wayland clipboard before cursor' global user 'P' '!wl-paste -n<ret>'
map -docstring 'paste wayland primary clipboard before cursor' global user ',' '!wl-paste -n -p<ret>'
map -docstring 'replace selections with wayland clipboard' global user 'R' '|wl-paste<ret>'
map -docstring 'copy last selection to wayland clipboard' global user 'y' '<a-|>wl-copy<ret>'

map global normal '<c-v>' '!wl-paste -n<ret>'
map global normal '<c-c>' '<a-|>wl-copy<ret>'
hook global NormalKey '<mouse:press:middle:.*>' 'execute-keys "!wl-paste -n -p<ret>"'
hook global User UpdatePrimaryClipboardNormal 'execute-keys "<a-|>wl-copy -p<ret>"'

map global insert '<c-v>' '<a-;>!wl-paste -n<ret>'
map global insert '<c-c>' '<a-;><a-|>wl-copy<ret>'
hook global InsertKey '<mouse:press:middle:.*>' 'execute-keys "<a-;>!wl-paste -n -p<ret>"'
hook global User UpdatePrimaryClipboardInsert 'execute-keys "<a-;><a-|>wl-copy -p<ret>"'


# hook into mouse multiclick
hook global User NormalDoubleClickPress 'trigger-user-hook UpdatePrimaryClipboardNormal'
hook global User NormalTripleClickPress 'trigger-user-hook UpdatePrimaryClipboardNormal'
hook global User InsertDoubleClickPress 'trigger-user-hook UpdatePrimaryClipboardInsert'
hook global User InsertTripleClickPress 'trigger-user-hook UpdatePrimaryClipboardInsert'

